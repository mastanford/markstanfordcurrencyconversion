package com.currency.badoocurrency;

import android.app.Application;

/**
 * Created by m.stanford on 4/5/16.
 */
public class BadooApp extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }
}
