package com.currency.badoocurrency;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.currency.badoocurrency.fragments.ProductFragment;
import com.currency.badoocurrency.fragments.TransactionFragment;

public class MainActivity extends AppCompatActivity implements ProductFragment.OnListFragmentInteractionListener, FragmentManager.OnBackStackChangedListener{

    private static final String TAG = MainActivity.class.getSimpleName();

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();

        initUI();
    }

    private void initUI() {
        Fragment productFragment = ProductFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_main_content, productFragment).commit();
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        setHomeToolbar();
    }

    private void setHomeToolbar() {
        setTitle("Products");
        setToolBarTitle("Products");
        setToolbarLogo(R.drawable.home);
        setToolbarOnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    private void setTransactionToolbar(String sku){
        setTitle("Transactions");
        setToolBarTitle("Transactions for " + sku);
        setToolbarLogo(R.drawable.back);
        setToolbarOnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setProductFragment();
                setHomeToolbar();
            }
        });
    }

    private void setProductFragment() {
        Fragment productFragment = ProductFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_main_content, productFragment).commit();
    }

    private void setTransactionFragment(String sku){
        Fragment transactionFragment = TransactionFragment.newInstance(sku);
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_main_content, transactionFragment).addToBackStack("home").commit();
    }

    private void setToolBarTitle(String title){
        toolbar.setTitle(title);
    }

    private void setToolbarLogo(int resource){
        toolbar.setNavigationIcon(resource);
    }

    private void setToolbarOnClick(View.OnClickListener listener){
        toolbar.setNavigationOnClickListener(listener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_products, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListFragmentInteraction(String sku) {
        setTransactionFragment(sku);
        setTransactionToolbar(sku);
    }

    @Override
    public void onBackStackChanged() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fl_main_content);
        if (currentFragment instanceof ProductFragment) {
            setHomeToolbar();
        }
    }
}
