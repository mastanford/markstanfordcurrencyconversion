package com.currency.badoocurrency.currencyUtils;

import android.content.Context;
import android.util.Log;

import com.currency.badoocurrency.data.DataManager;
import com.currency.badoocurrency.models.Rate;
import com.currency.badoocurrency.models.Rates;
import com.currency.badoocurrency.models.Transaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by m.stanford on 4/5/16.
 */
public class CurrencyUtil {

    private static final String TAG = CurrencyUtil.class.getSimpleName();

    /**
     * Basic path finding algo using edges. To - From is a edge.
     * I will assume that the we can only use To - From and not From - To.
     * After finding one path, I will add that conversion rate to the conversion rate list stored in volatile.
     *
     * @param amount
     * @param rates
     * @return
     */
    public static float convertCurrency(Context context, Currency from, Currency to, float amount, Rates rates){

        //Check if we need to convert
        if(from.equals(to)){
            return amount;
        }

        //We need to convert so find a conversion path.
        List<Node> path = getConversionRate(rates, from, to);

        //We have an empty path which means we did not find a conversion.  -1 will be our flag for that
        if(path.isEmpty()){
            return -1;
        }

        //Simple memoization.  Adding paths we find to the path list, hopefully this math is correct
        if(path.size() > 1){
            float fac = 1;
            for(Node node : path){
                fac *= node.rate;
            }
            DataManager.getInstance(context).getRates().getRateList().add(new Rate(from.getCurrencyCode(), to.getCurrencyCode(), fac));
        }

        //Calc the conversion, hopefully this math is correct
        for(Node node : path){
            amount *= node.rate;
        }

        return amount;
    }

    public static float calculateTotal(Context context, Currency to, List<Transaction> transactions, Rates rates){
        float total = 0;
        for(Transaction transaction : transactions){
            total += convertCurrency(context, Currency.getInstance(transaction.getCurrency()), to, transaction.getAmount(), rates);
        }
        return total;
    }

    /**
     * Iterative solution to find a path from start to end and return that path, returns empty if no path.
     */
    private static List<Node> getConversionRate(Rates rates, Currency from, Currency to){
        List<Node> path = new ArrayList<>();
        List<Node> nodeList = buildNodes(rates);

        //Base case if there is a to/from in one node.
        for (Node node : nodeList){
            if(node.from.equals(from.toString()) && node.to.equals(to.toString())){
                path.add(node);
                return path;
            }
        }

        //Otherwise lets do bfs
        return findConversionPath(nodeList, from, to);
    }

    /**
     * Lets start from the to and work to the from.
     * @param nodeList
     * @param from
     * @param to
     * @return
     */
    private static List<Node> findConversionPath(List<Node> nodeList, Currency from, Currency to) {
        List<Node> path = new ArrayList<>();
        Queue<Node> queue = new LinkedList<>();

        Log.d(TAG, "findConversionPath Checking : from: " + from + " to: " + to);

        //Push all valid starting points onto the stack, no parent since this is a valid from.
        for(Node node : getStartingNodes(from, nodeList)) {
            Log.d(TAG, "From Pushing : from: " + node.from + " to: " + node.to);
            queue.add(node);
        }

        //Keep going through all valid starting points and their valid connected nodes and so on until we reach the TO or the end
        Node node;
        List<Node> visited = new ArrayList<>();
        while(!queue.isEmpty()){
            node = queue.poll();
            visited.add(node);

            Log.d(TAG, "Checking : from: " + node.from + " to: " + node.to);

            if(node.to.equalsIgnoreCase(to.toString())){
                return buildPath(node);
            }else{
                for (Node temp : node.toEdges) {
                    if(!visited.contains(temp)) {
                        Log.d(TAG, "temp Pushing from: " + temp.from + " to: " + temp.to);
                        temp.parent = node;
                        queue.add(temp);
                    }
                }
            }
        }

        //Means we fell through our bfs and we need return an empty path
        return path;
    }

    /**
     * Takes a node that we know has the proper ending currency and then builds a path to the starting currency.
     * @param node
     * @return
     */
    private static List<Node> buildPath(Node node) {
        List<Node> path = new ArrayList<>();
        while(node.hasParent()){
            path.add(node);
            node = node.parent;
        }
        path.add(node);

        //Reverse the order
        Collections.reverse(path);
        for(Node node1: path){
            Log.d(TAG, String.format("From %s to %s", node1.from, node1.to));
        }

        return path;
    }

    /**
     * Builds a list of nodes from the rates.
     * The nodes will have a reference to each node that has an end or start with this node.
     * @param rates
     * @return
     */
    private static List<Node> buildNodes(Rates rates) {
        List<Node> nodes = new ArrayList<>();

        //Make node for each rate
        Node node;
        for (Rate rate : rates.getRateList()) {
            //Build the node
            node = new Node(rate);
            nodes.add(node);
        }

        //Build edges
        for (Node temp : nodes) {
            temp.toEdges = getAllToNodes(Currency.getInstance(temp.to), nodes);
            temp.fromEdges = getAllFromNodes(Currency.getInstance(temp.from), nodes);
        }

        return nodes;
    }

    /**
     * Gets a list of all nodes that go from - to with our currency as the to
     * @param to
     * @param nodeList
     * @return
     */
    private static List<Node> getAllToNodes(Currency to, List<Node> nodeList) {
        List<Node> list = new ArrayList<>();
        for(Node node: nodeList){
            if(node.from.equalsIgnoreCase(to.toString())){
                list.add(node);
            }
        }
        return list;
    }

    /**
     * Gets a list of all nodes that go from - to with our currency as the from
     * @param from
     * @param nodeList
     * @return
     */
    private static List<Node> getAllFromNodes(Currency from, List<Node> nodeList) {
        List<Node> list = new ArrayList<>();
        for(Node node: nodeList){
            if(node.to.equalsIgnoreCase(from.toString())){
                list.add(node);
            }
        }
        return list;
    }

    /**
     * Gets all nodes that share a from with our from
     * @param from
     * @param nodeList
     * @return
     */
    private static List<Node> getStartingNodes(Currency from, List<Node> nodeList) {
        List<Node> list = new ArrayList<>();
        for(Node node: nodeList){
            if(node.from.equalsIgnoreCase(from.toString())){
                list.add(node);
            }
        }
        return list;
    }

    static class Node{
        float rate;
        String to;
        String from;
        List<Node> toEdges = new ArrayList<>();
        public List<Node> fromEdges = new ArrayList<>();
        Node parent;

        public Node(Rate rate) {
            this.rate = rate.getRate();
            this.to = rate.getTo();
            this.from = rate.getFrom();
        }

        boolean hasParent(){
            return parent != null;
        }
    }
}
