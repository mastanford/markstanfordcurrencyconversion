package com.currency.badoocurrency.data;

import android.content.Context;
import android.util.Log;

import com.currency.badoocurrency.R;
import com.currency.badoocurrency.models.Rate;
import com.currency.badoocurrency.models.Rates;
import com.currency.badoocurrency.models.Transaction;
import com.currency.badoocurrency.models.Transactions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by m.stanford on 4/5/16.
 */
public class DataManager {
    private static final String TAG = DataManager.class.getSimpleName();

    private Context context;
    private Rates rates;
    private Transactions transactions;

    private static DataManager INSTANCE;

    public static DataManager getInstance(Context context) {
        if(INSTANCE == null){
            INSTANCE = new DataManager(context);
        }

        return INSTANCE;
    }

    private DataManager(Context context) {
        //rates
        InputStream is = context.getResources().openRawResource(R.raw.rates1);
        String json = null;
        try {
            json = getStringFromIS(is);
            Log.d(TAG, json);
            List<Rate> tempRates = new Gson().fromJson(json, new TypeToken<List<Rate>>(){}.getType());
            this.rates = new Rates(tempRates);
        } catch (IOException e) {
            rates = new Rates();
            Log.e(TAG, "Could not parse rates1 json");
        }

        //transactions
        is = context.getResources().openRawResource(R.raw.transactions1);
        try {
            json = getStringFromIS(is);
            List<Transaction> temptrans = new Gson().fromJson(json, new TypeToken<List<Transaction>>(){}.getType());
            this.transactions = new Transactions(temptrans);
        } catch (IOException e) {
            transactions = new Transactions();
            Log.e(TAG, "Could not parse transactions1 json");
        }
    }

    /**
     * Use for testing new data sets
     * @param transactionsResource
     * @param ratesResource
     * @return
     */
    public DataManager loadDummyData(int transactionsResource, int ratesResource){
        //rates
        InputStream is = context.getResources().openRawResource(ratesResource);
        String json = null;
        try {
            json = getStringFromIS(is);
            List<Rate> tempRates = new Gson().fromJson(json, new TypeToken<List<Rate>>(){}.getType());
            this.rates = new Rates(tempRates);
        } catch (IOException e) {
            rates = new Rates();
            Log.e(TAG, "Could not parse ratesResource json");
        }

        //transactions
        is = context.getResources().openRawResource(transactionsResource);
        try {
            json = getStringFromIS(is);
            List<Transaction> temptrans = new Gson().fromJson(json, new TypeToken<List<Transaction>>(){}.getType());
            this.transactions = new Transactions(temptrans);
        } catch (IOException e) {
            transactions = new Transactions();
            Log.e(TAG, "Could not parse transactionsResource json");
        }

        return this;
    }

    private String getStringFromIS(InputStream inputStream) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line);
        }
        return total.toString();
    }

    public List<Transaction> getTransactionsBySKU(String sku) {
        List<Transaction> list = new ArrayList<>();
        for(Transaction transaction : transactions.getTransactionList()){
            if(transaction.getStockKeepingUnit().equals(sku)){
                list.add(transaction);
            }
        }
        return list;
    }

    public List<String> getSkus(){
        Set<String> skus = new HashSet<>();
        for(Transaction transaction : transactions.getTransactionList()){
            skus.add(transaction.getStockKeepingUnit());
        }
        return new ArrayList<>(skus);
    }

    public Transactions getTransactions() {
        return transactions;
    }

    public Rates getRates(){
        return rates;
    }
}
