package com.currency.badoocurrency.fragments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.currency.badoocurrency.R;
import com.currency.badoocurrency.data.DataManager;

import java.util.List;

public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<ProductRecyclerViewAdapter.ViewHolder> {

    private final List<String> skus;
    private final ProductFragment.OnListFragmentInteractionListener mListener;
    private Context context;

    public ProductRecyclerViewAdapter(List<String> skus, ProductFragment.OnListFragmentInteractionListener listener) {
        this.skus = skus;
        java.util.Collections.sort(this.skus);
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.sku = skus.get(position);
        holder.tv_sku.setText(holder.sku);
        long numTransactions = DataManager.getInstance(context).getTransactionsBySKU(holder.sku).size();
        holder.num.setText(numTransactions + " transactions");

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.sku);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return skus.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tv_sku;
        public final TextView num;
        public String sku;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tv_sku = (TextView) view.findViewById(R.id.tv_products_sku);
            num = (TextView) view.findViewById(R.id.tv_products_num_transactions);
        }
    }
}
