package com.currency.badoocurrency.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.currency.badoocurrency.R;
import com.currency.badoocurrency.currencyUtils.CurrencyUtil;
import com.currency.badoocurrency.data.DataManager;
import com.currency.badoocurrency.models.Transaction;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;

public class TransactionFragment extends Fragment {
    private static final String TAG = TransactionFragment.class.getSimpleName();

    private static final String ARG_SKU = "sku";
    private String sku;

    public TransactionFragment() {
    }

    public static TransactionFragment newInstance(String sku) {
        TransactionFragment fragment = new TransactionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SKU, sku);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            sku = getArguments().getString(ARG_SKU);
        }else{
            Log.e(TAG, "No SKU in Args");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transaction_list, container, false);

        Context context = view.getContext();
        List<Transaction> transactions = DataManager.getInstance(context).getTransactionsBySKU(sku);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(new TransactionRecyclerViewAdapter(context, transactions));

        Currency gbp = Currency.getInstance("GBP");
        float total = CurrencyUtil.calculateTotal(context, gbp, transactions, DataManager.getInstance(context).getRates());
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        formatter.setCurrency(Currency.getInstance("GBP"));
        String output = String.format("Total: %s", formatter.format(total));
        ((TextView) view.findViewById(R.id.tv_transactions_total)).setText(output);

        return view;
    }
}
