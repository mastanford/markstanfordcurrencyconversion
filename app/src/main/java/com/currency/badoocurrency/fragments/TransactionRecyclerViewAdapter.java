package com.currency.badoocurrency.fragments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.currency.badoocurrency.R;
import com.currency.badoocurrency.currencyUtils.CurrencyUtil;
import com.currency.badoocurrency.data.DataManager;
import com.currency.badoocurrency.models.Transaction;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;

public class TransactionRecyclerViewAdapter extends RecyclerView.Adapter<TransactionRecyclerViewAdapter.ViewHolder> {

    private final List<Transaction> transactions;
    private Context context;

    public TransactionRecyclerViewAdapter(Context context, List<Transaction> transactionsForSKU) {
        this.context = context;
        this.transactions = transactionsForSKU;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_transaction, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.transaction = transactions.get(position);
        Currency localCurrencyObj = Currency.getInstance(holder.transaction.getCurrency());
        Currency gbp = Currency.getInstance("GBP");
        float amount = holder.transaction.getAmount();
        float exhange = CurrencyUtil.convertCurrency(context, localCurrencyObj, gbp, amount, DataManager.getInstance(context).getRates());
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        formatter.setCurrency(Currency.getInstance("GBP"));
        holder.GBP.setText((exhange > 0 ? formatter.format(exhange) : "--"));
        formatter = NumberFormat.getCurrencyInstance();
        formatter.setCurrency(Currency.getInstance(localCurrencyObj.getCurrencyCode()));
        holder.LOCAL.setText(formatter.format(amount));
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final TextView GBP;
        public final TextView LOCAL;
        public Transaction transaction;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            GBP = (TextView) view.findViewById(R.id.tv_transactions_gbp);
            LOCAL = (TextView) view.findViewById(R.id.tv_transactions_local);
        }
    }
}
