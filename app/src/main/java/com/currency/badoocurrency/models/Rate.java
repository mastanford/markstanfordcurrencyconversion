package com.currency.badoocurrency.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by m.stanford on 4/5/16.
 */
public class Rate {
    @SerializedName("from")
    private String from;
    @SerializedName("to")
    private String to;
    @SerializedName("rate")
    private float rate;

    public Rate() {
    }

    public Rate(String from, String to, float rate) {
        this.from = from;
        this.to = to;
        this.rate = rate;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Rate{" +
                "from=" + from +
                ", to=" + to +
                ", rate=" + rate +
                '}';
    }
}
