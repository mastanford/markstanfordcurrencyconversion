package com.currency.badoocurrency.models;

import java.util.List;

/**
 * Created by m.stanford on 4/5/16.
 * Wrapper around rates
 */
public class Rates {

    private List<Rate> rateList;

    public Rates() {
    }

    public Rates(List<Rate> rateList) {
        this.rateList = rateList;
    }

    public List<Rate> getRateList() {
        return rateList;
    }

    public void setRateList(List<Rate> rateList) {
        this.rateList = rateList;
    }
}
