package com.currency.badoocurrency.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by m.stanford on 4/5/16.
 */
public class Transaction {
    @SerializedName("amount")
    private float amount;

    @SerializedName("sku")
    private String stockKeepingUnit;

    @SerializedName("currency")
    private String currency;

    public Transaction() {
    }

    public Transaction(float amount, String stockKeepingUnit, String currency) {
        this.amount = amount;
        this.stockKeepingUnit = stockKeepingUnit;
        this.currency = currency;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getStockKeepingUnit() {
        return stockKeepingUnit;
    }

    public void setStockKeepingUnit(String stockKeepingUnit) {
        this.stockKeepingUnit = stockKeepingUnit;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "amount=" + amount +
                ", stockKeepingUnit='" + stockKeepingUnit + '\'' +
                ", currency=" + currency +
                '}';
    }
}
