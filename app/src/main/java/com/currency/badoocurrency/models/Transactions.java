package com.currency.badoocurrency.models;

import java.util.List;

/**
 * Created by m.stanford on 4/5/16.
 * Wrapper around transactions
 */
public class Transactions {

    private List<Transaction> transactionList;

    public Transactions() {
    }

    public Transactions(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }
}
